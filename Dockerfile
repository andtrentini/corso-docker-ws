FROM node:latest
WORKDIR /app
copy . /app
RUN npm install
RUN npm install -g nodemon