const express = require('express');
const cors = require('cors');
const compression = require('compression');
const mysql = require('mysql');
const bodyParser = require('body-parser');

var service = express();

// Configurazione dei servizi middleware
service.use(cors());
service.use(compression());
service.use(bodyParser.json());
service.use(bodyParser.urlencoded({extended: false}));

service.get('/', (request, response) => {
    response.sendFile(__dirname+'/index.html');
});

// web service

const config = {
    host: 'corso-docker-mysql',
    user: 'root',
    password: 'root',
    database: 'cars'
}

service.get('/owners', (request, response) => {
    // Stabilire una connessione con mysql
    let connection = mysql.createConnection(config);

    // Definire l'istruzione SQL
    let sqlString = 'SELECT * FROM owners';
    connection.query(sqlString, (error, data) => {
        if (!error) {
            response.json(data);
        }
        else {
            response.status(500).send(error);
        }
    })
});

service.get('/owners/:id', (request, response) => {
    let id = request.params.id;
    // Stabilire una connessione con mysql
    let connection = mysql.createConnection(config);

    // Definire l'istruzione SQL
    let sqlString = 'SELECT * FROM owners WHERE id=?';
    connection.query(sqlString, id, (error, data) => {
        if (!error) {
            response.json(data);
        }
        else {
            response.status(500).send(error);
        }
    })
});

service.delete('/owners/:id', (request, response) => {
    let id = request.params.id;
    // Stabilire una connessione con mysql
    let connection = mysql.createConnection(config);

    // Definire l'istruzione SQL
    let sqlString = 'DELETE FROM owners WHERE id=?';
    connection.query(sqlString, id, (error, data) => {
        if (!error) {
            response.json(data);
        }
        else {
            response.status(500).send(error);
        }
    })
});

service.post('/owners', (request, response) => {
    let dati = request.body;
    console.log(dati);
    let sqlparams = [dati.id, dati.first_name, dati.last_name, dati.email, dati.gender];
    let sqlString = "INSERT INTO owners VALUES (?,?,?,?,?);";
    let connection = mysql.createConnection(config);
    
    connection.query(sqlString, sqlparams, (error, data) => {
        if (!error) {
            response.json(data);
        }
        else {
            response.status(500).send(error);
        }
    });    
});

service.put('/owners/:id', (request, response) => {
    let id = request.params.id;
    let dati = request.body;
    let sqlparams = [dati.id, dati.first_name, dati.last_name, dati.email, dati.gender, id];    
    let sqlString = "UPDATE owners SET id=?, first_name=?, last_name=?, email=?, gender=? WHERE id=?";
    
    let connection = mysql.createConnection(config);
    
    connection.query(sqlString, sqlparams, (error, data) => {
        if (!error) {
            response.json(data);
        }
        else {
            response.status(500).send(error);
        }
    }); 
})

var server = service.listen(3000, () => {
    console.log('Server in ascolto sulla porta 3000...');
})